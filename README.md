# Feature Selection Tool

I tried to add new features to WillKoehrsen's Feature Selection Tool (https://github.com/WillKoehrsen/feature-selector) for my dataset.

Firstly I added all dataset path to in a list and then also add possibly best parameters to other two list which are 'corrValues' and 'cumValues'.

My ideea was like a grid search.

In every tried, created automatically a new csv file with different parameter for one dataset. Also, end of the one dataset's operations(tried all parameters like grid search) created excell automaticaly and I could see the best parameter in this excel file. As a result, I was able to select the most accurate feature selection dataset via excel file.